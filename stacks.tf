variable "aws_account_id" {}
variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "aws_region" {}
variable "heroku_email" {}
variable "heroku_api_key" {}
variable "cloudflare_email" {}
variable "cloudflare_token" {}
variable "spotify_client_id" {}
variable "spotify_client_secret" {}
variable "domain" {
  default = "vincenttunru.com"
}

data "template_file" "subdomain" {
  template = "${terraform.workspace == "master" ? "$${production}" : "$${review}"}"

  vars {
    production = "djteam"
    review = "${terraform.workspace}-review-djteam"
  }
}

terraform {
  backend "s3" {
    bucket = "terraform-remote-state-djteam"
    key = "terraform.tfstate"
    encrypt = true
  }
}

provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
  region = "${var.aws_region}"
}

provider "heroku" {
  email = "${var.heroku_email}"
  api_key = "${var.heroku_api_key}"
}

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

resource "heroku_app" "heroku_api_app" {
  name = "djt-${terraform.workspace}"
  region = "eu"
  buildpacks = [
    "heroku/nodejs"
  ]

  config_vars {
    SPOTIFY_CLIENT_ID = "${var.spotify_client_id}"
    SPOTIFY_CLIENT_SECRET = "${var.spotify_client_secret}"
    aws_account_id = "${var.aws_account_id}"
    aws_access_key_id = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"
    aws_region = "${var.aws_region}"
  }
}

output "heroku_git_url" {
  value = "${heroku_app.heroku_api_app.git_url}"
}
output "heroku_hostname" {
  value = "${heroku_app.heroku_api_app.heroku_hostname}"
}

resource "aws_s3_bucket" "frontend_bucket" {
  bucket = "${data.template_file.subdomain.rendered}.${var.domain}"
  acl = "public-read"
  # Allows deleting the bucket even when there are files in it:
  force_destroy = true
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowPublicRead",
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${data.template_file.subdomain.rendered}.${var.domain}/*",
      "Principal": "*"
    }
  ]
}
POLICY
  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "cloudflare_record" "subdomain" {
  domain  = "${var.domain}"
  name    = "${data.template_file.subdomain.rendered}"
  value   = "${aws_s3_bucket.frontend_bucket.website_endpoint}"
  type    = "CNAME"
  ttl     = 1 # 1: automatic
  proxied = true
}

output "s3_url" {
  value = "s3://${aws_s3_bucket.frontend_bucket.id}/"
}
output "live_url" {
  value = "https://${aws_s3_bucket.frontend_bucket.id}/"
}
