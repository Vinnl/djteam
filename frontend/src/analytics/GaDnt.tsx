import * as React from 'react';
import * as ReactGA from 'react-ga';

const doNotTrack = (typeof window !== 'undefined' && typeof window.navigator !== 'undefined' && window.navigator.doNotTrack === '1');

export const initialize = wrapFunction(ReactGA.initialize);
export const set = wrapFunction(ReactGA.set);
export const pageview = wrapFunction(ReactGA.pageview);
export const modalview = wrapFunction(ReactGA.modalview);
export const event = wrapFunction(ReactGA.event);
export const timing = wrapFunction(ReactGA.timing);
export const ga = wrapFunction(ReactGA.ga);
export const outboundLink = wrapFunction(ReactGA.outboundLink);
export const exception = wrapFunction(ReactGA.exception);

export class OutboundLink extends ReactGA.OutboundLink {
  public render() {
  if (doNotTrack) {
    return (<a href={this.props.to} target={this.props.target}>{this.props.children}</a>);
  }

  return <ReactGA.OutboundLink to={this.props.to} eventLabel={this.props.eventLabel} target={this.props.target}/>;
  }
}

// tslint:disable-next-line:ban-types
function wrapFunction<T extends Function>(functionToWrap: T): T {
  if (doNotTrack) {
    return (() => undefined) as any;
  }

  return functionToWrap;
}
