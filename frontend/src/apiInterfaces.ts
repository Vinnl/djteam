export type UserId = number;
export type SongId = string;
export type PlaylistId = string;
export type Queue = SongId[];
export type PartyId = number;
export type Token = string;
export type SpotifyAccessToken = string;
export type SpotifyAuthCode = string;

export interface SongDetails {
  id: SongId;
  durationMs: number;
  name: string;
  artists: string[];
  cover?: string;
}
export interface PlaylistDetails {
  id: PlaylistId;
  numberOfSongs: number;
  name: string;
  cover?: string;
}
export interface Party {
  partyId: PartyId;
  host: UserId;
  guests: UserId[];
  current?: UserId;
  queue: Queue;
  userQueues: { [ userId: number ]: Queue };
  trackDetails: { [ songId: string ]: SongDetails };
}
export interface SearchResult {
  id: SongId;
  title: string;
  artists: string[];
  durationMs: number;
  cover?: string;
}

export interface ReconnectRequest {
  type: 'reconnect';
  token: Token;
};
export interface PingRequest {
  type: 'ping';
};
export interface RecommendationRequest {
  type: 'recommendation';
  seeds: SongId[];
};
export interface SearchRequest {
  type: 'search';
  query: string;
};
export interface NextRequest {
  type: 'next';
  prevSongId: SongId;
  songId?: SongId;
  partyId: PartyId;
};
export interface QueuePlaylistRequest {
  type: 'queuePlaylist';
  partyId: PartyId;
  playlistId: PlaylistId;
};
export interface QueueRequest {
  type: 'queue';
  songId: SongId;
};
export interface JoinRequest {
  type: 'join';
  partyId: PartyId;
};
export interface HostRequest {
  type: 'host';
  spotifyAuth: {
    code: SpotifyAuthCode;
    redirectUri: string
  }
};

export type WebSocketRequest = ReconnectRequest | PingRequest | RecommendationRequest | SearchRequest | NextRequest | QueuePlaylistRequest | QueueRequest | HostRequest | JoinRequest;

export function isReconnectRequest(message: WebSocketRequest): message is ReconnectRequest {
  return (message as ReconnectRequest).type === 'reconnect';
}
export function isPingRequest(message: WebSocketRequest): message is PingRequest {
  return (message as PingRequest).type === 'ping';
}
export function isRecommendationRequest(message: WebSocketRequest): message is RecommendationRequest {
  return (message as RecommendationRequest).type === 'recommendation';
}
export function isSearchRequest(message: WebSocketRequest): message is SearchRequest {
  return (message as SearchRequest).type === 'search';
}
export function isNextRequest(message: WebSocketRequest): message is NextRequest {
  return (message as NextRequest).type === 'next';
}
export function isQueuePlaylistRequest(message: WebSocketRequest): message is QueuePlaylistRequest {
  return (message as QueuePlaylistRequest).type === 'queuePlaylist';
}
export function isQueueRequest(message: WebSocketRequest): message is QueueRequest {
  return (message as QueueRequest).type === 'queue';
}
export function isJoinRequest(message: WebSocketRequest): message is JoinRequest {
  return (message as JoinRequest).type === 'join';
}
export function isHostRequest(message: WebSocketRequest): message is HostRequest {
  return (message as HostRequest).type === 'host';
}

export interface ConnectionResponse {
  type: 'connection';
  token: Token;
  userId: UserId;
}
export interface ReconnectResponse {
  type: 'reconnect';
  userId: UserId;
  party?: Party;
}
export interface TokenRefreshResponse {
  type: 'tokenRefresh';
  spotifyAccessToken: SpotifyAccessToken;
}
export interface RecommendationResponse {
  type: 'recommendation';
  results: SongDetails[];
};
export interface SearchResponse {
  type: 'search';
  results: SearchResult[];
};
export interface HostResponse {
  type: 'host';
  party: Party;
  spotifyAccessToken: SpotifyAccessToken;
  playlists: PlaylistDetails[];
};
export interface PartyUpdatedResponse {
  type: 'partyUpdate';
  party: Party;
  role: 'participant' | 'host';
}
export interface EndResponse {
  type: 'end';
  partyId: PartyId;
};

export type WebSocketResponse = ConnectionResponse | ReconnectResponse | RecommendationResponse | SearchResponse | TokenRefreshResponse | HostResponse | PartyUpdatedResponse | EndResponse;

export function isConnectionResponse(message: WebSocketResponse): message is ConnectionResponse {
  return (message as ConnectionResponse).type === 'connection';
}
export function isReconnectResponse(message: WebSocketResponse): message is ReconnectResponse {
  return (message as ReconnectResponse).type === 'reconnect';
}
export function isRecommendationResponse(message: WebSocketResponse): message is RecommendationResponse {
  return (message as RecommendationResponse).type === 'recommendation';
}
export function isSearchResponse(message: WebSocketResponse): message is SearchResponse {
  return (message as SearchResponse).type === 'search';
}
export function isTokenRefreshResponse(message: WebSocketResponse): message is TokenRefreshResponse {
  return (message as TokenRefreshResponse).type === 'tokenRefresh';
}
export function isHostResponse(message: WebSocketResponse): message is HostResponse {
  return (message as HostResponse).type === 'host';
}
export function isPartyUpdatedResponse(message: WebSocketResponse): message is PartyUpdatedResponse {
  return (message as PartyUpdatedResponse).type === 'partyUpdate';
}
export function isEndResponse(message: WebSocketResponse): message is EndResponse {
  return (message as EndResponse).type === 'end';
}
