import * as React from 'react';

import 'bulma/css/bulma.min.css';
import './Spinner.css';

import * as ReactGA from './analytics/GaDnt';
import {
  HostRequest,
  isConnectionResponse,
  isEndResponse,
  isHostResponse,
  isPartyUpdatedResponse,
  isReconnectResponse,
  isTokenRefreshResponse,
  JoinRequest,
  Party,
  ReconnectRequest,
  SongId,
  SpotifyAccessToken,
  Token,
  UserId,
  WebSocketRequest,
  WebSocketResponse,
  PlaylistDetails,
} from './apiInterfaces';
import { Guest } from './guest/Guest';
import { Host } from './host/Host';

interface Props {}
interface State {
  form: {
    joinId: string;
  };
  queue: SongId[];
  delayPassed: boolean;
  messageQueue: WebSocketRequest[];
  spotifyPlaylists: PlaylistDetails[];
  userId?: UserId;
  role?: 'participant' | 'host';
  party?: Party;
  socket?: WebSocket;
  token?: Token,
  spotifyToken?: SpotifyAccessToken;
  connectionCheckTimer?: number;
}

class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { form: { joinId: '' }, queue: [], delayPassed: false, messageQueue: [], spotifyPlaylists: [] };

    this.initialiseSocket = this.initialiseSocket.bind(this);
    this.onChangeJoinId = this.onChangeJoinId.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
    this.handleJoin = this.handleJoin.bind(this);
    this.join = this.join.bind(this);
    this.host = this.host.bind(this);
    this.ensureConnection = this.ensureConnection.bind(this);
  }

  public componentDidMount() {
    this.initialiseSocket();

    this.setState({ connectionCheckTimer: window.setInterval(this.ensureConnection, 5000) });

    if(typeof process.env.REACT_APP_GA_TRACKING_CODE === 'string') {
      // tslint:disable:object-literal-sort-keys
      const gaDimensions = {
        GA_CONFIG_VERSION: 'dimension1',
        CODE_BRANCH: 'dimension2',
      };
      // tslint:enable:object-literal-sort-keys

      ReactGA.initialize(process.env.REACT_APP_GA_TRACKING_CODE);
      ReactGA.set({
        // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#loading-analyticsjs
        transport: 'beacon',
        // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#tracking-custom-data
        [gaDimensions.GA_CONFIG_VERSION]: '1',
        [gaDimensions.CODE_BRANCH]: process.env.REACT_APP_CODE_BRANCH,
      });
      ReactGA.pageview('/');
    }
  }

  public componentWillUnmount() {
    window.clearInterval(this.state.connectionCheckTimer);
  }

  public ensureConnection() {
    if (typeof this.state.socket !== 'undefined' && this.state.socket.readyState !== WebSocket.OPEN) {
      this.initialiseSocket();
    }
  }

  public initialiseSocket() {
    const url = process.env.REACT_APP_BACKEND_WEBSOCKET_URL || `wss://${document.location.host}`;

    const socket = new WebSocket(url);
    socket.onopen = () => {
      this.setState({ socket });

      // If we were already connected before, try to pick up that connection again:
      if (typeof this.state.token !== 'undefined') {
        const data: ReconnectRequest = {
          token: this.state.token,
          type: 'reconnect',
        };
        socket.send(JSON.stringify(data));
      } else {
        // If we landed here after Spotify's authorisation flow,
        // pass the access token to the server and start hosting.
        const authCodes = /code=((?:\w|-|_)+)(?:&|$)/.exec(document.location.search);
        if (Array.isArray(authCodes) && authCodes.length === 2) {
          const spotifyAuthCode = authCodes[1];
          this.host(spotifyAuthCode);
        }
      }
    };

    socket.onmessage = (message) => {
      const data: WebSocketResponse = JSON.parse(message.data);

      if (isConnectionResponse(data)) {
        // Only store the token if we don't already have one - otherwise we're probably reconnecting
        if (typeof this.state.token === 'undefined') {
          this.setState({ userId: data.userId, token: data.token });
        }
      }
      if (isReconnectResponse(data)) {
        this.setState({ userId: data.userId, party: data.party });

        // Send all WebSocket messages that could not be sent while we were disconnected:
        this.state.messageQueue.forEach((queuedMessage) => socket.send(JSON.stringify(queuedMessage)));
        this.setState({ messageQueue: [] })
      }
      if (isTokenRefreshResponse(data)) {
        this.setState({ spotifyToken: data.spotifyAccessToken });
      }
      if (isPartyUpdatedResponse(data)) {
        this.setState({
          party: data.party,
          role: data.role,
        });
      }
      if (isHostResponse(data)) {
        this.setState({ party: data.party, role: 'host', spotifyToken: data.spotifyAccessToken, spotifyPlaylists: data.playlists });
      }
      if(isEndResponse(data)) {
        this.setState({ party: undefined, spotifyToken: undefined });
      }
    };

    setTimeout(() => this.setState({ delayPassed: true }), 200);
  }

  public render() {
    if (typeof this.state.socket === 'undefined') {
      if (!this.state.delayPassed) {
        // If the app happens to load within 200ms, don't show the spinner
        // - it will only make it look slower.
        return null;
      }

      return (
        <section className="section">
          <div className="container">
            <p className="title has-text-centered">Connecting to the server, one moment please&hellip;</p>
            <div className="spinner"/>
          </div>
        </section>
      );
    }

    return (
      <>
        <header className="navbar">
          <div className="container">
            <div className="navbar-brand">
              <div className="navbar-item">
                <h1 className="is-size-4">DJ Team</h1>
              </div>
            </div>
            {this.renderPartyCode()}
          </div>
        </header>
        {this.renderMenu()}
        {this.renderHost()}
        {this.renderParticipant()}
      </>
    );
  }

  public renderPartyCode() {
    if (typeof this.state.party === 'undefined') {
      return null;
    }

    return (
      <div className="navbar-end">
        <div className="navbar-item">
          Party:&nbsp;<strong>{this.state.party.partyId}</strong>
        </div>
      </div>
    );
  }

  public renderMenu() {
    if (typeof this.state.party !== 'undefined') {
      return null;
    }

    const spotifyScopes =
      [
        'user-read-currently-playing',
        'user-modify-playback-state',
        'user-read-playback-state',
        'streaming',
        'user-read-birthdate',
        'user-read-email',
        'user-read-private',
        'playlist-read-private',
        'playlist-read-collaborative',
      ].join('%20');

    return (
      <div className="section">
        <div className="container">
          <div className="has-text-centered">
            <p>
              <a
                href={`https://accounts.spotify.com/authorize?client_id=${process.env.REACT_APP_SPOTIFY_CLIENT_ID}&redirect_uri=${document.location.origin}&scope=${spotifyScopes}&response_type=code`}
                className="button is-large"
                title="Host your own party"
              >Host a DJ Team</a>
            </p>
            <hr/>
            <form onSubmit={this.handleJoin} style={{ display: 'inline-block' }}>
              <label className="label is-large" htmlFor="joinId">&hellip;or join a team:</label>
              <div className="field has-addons">
                <div className="control">
                  <input
                    type="number"
                    className="input is-large"
                    onChange={this.onChangeJoinId}
                    name="joinId"
                    id="joinId"
                    min={0}
                    autoFocus={true}
                    placeholder="000"
                  />
                </div>
                <div className="control">
                  <input type="submit" className="button is-large" value="Join"/>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

  public renderParticipant() {
    if (
      typeof this.state.socket === 'undefined'
      || typeof this.state.role === 'undefined'
      || this.state.role !== 'participant'
      || typeof this.state.party === 'undefined'
      || typeof this.state.userId === 'undefined'
    ) {
      return null;
    }

    return (
      <Guest party={this.state.party} userId={this.state.userId} socket={this.state.socket} sendRequest={this.sendRequest}/>
    );
  }

  public renderHost() {
    if (
      typeof this.state.socket === 'undefined'
      || typeof this.state.role === 'undefined'
      || this.state.role !== 'host'
      || typeof this.state.party === 'undefined'
      || typeof this.state.spotifyToken === 'undefined'
    ) {
      return null;
    }

    return <Host
      party={this.state.party}
      sendRequest={this.sendRequest}
      spotifyToken={this.state.spotifyToken}
      spotifyPlaylists={this.state.spotifyPlaylists}
    />;
  }

  public sendRequest(request: WebSocketRequest) {
    if (typeof this.state.socket === 'undefined' || this.state.socket.readyState !== WebSocket.OPEN) {
      return this.setState(prevState => ({ messageQueue: prevState.messageQueue.concat([ request ]) }));
    }

    this.state.socket.send(JSON.stringify(request));
  }

  public handleJoin(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    this.join();
  }

  public join() {
    if (!this.state.socket) {
      return;
    }

    const partyId = Number.parseInt(this.state.form.joinId, 10);

    const message: JoinRequest = {
      partyId,
      type: 'join',
    };

    this.sendRequest(message);

    ReactGA.pageview(`/join/${partyId}`);
  }

  public host(spotifyAuthCode: string) {
    if (!this.state.socket) {
      return;
    }

    const message: HostRequest = {
      spotifyAuth: { code: spotifyAuthCode, redirectUri: document.location.origin },
      type: 'host',
    };

    this.sendRequest(message);

    ReactGA.pageview('/host');
  }

  public onChangeJoinId(event: React.ChangeEvent<HTMLInputElement>) {
    event.preventDefault();
    const joinId = event.target.value;

    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        joinId,
      },
    }));
  }
}

export default App;
