import * as React from 'react';

import {
  isRecommendationResponse,
  isSearchResponse,
  RecommendationRequest,
  SearchRequest,
  SearchResult,
  SongDetails,
  SongId,
  WebSocketRequest,
  WebSocketResponse,
} from '../apiInterfaces';
import { Search } from '../search/Search';
import * as ReactGA from '../analytics/GaDnt';
import unknownCoverImage from '../images/unknown-cover.svg';

interface Props {
  partyQueue: SongId[];
  ownQueue: SongId[];
  onSelect: (songId: SongId) => void;
  sendRequest: (request: WebSocketRequest) => void;
  socket: WebSocket;
}
interface State {
  tab: 'search' | 'suggestions';
  recommendations?: SongDetails[];
  searchResults?: SearchResult[];
}

export class AddPanel extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { tab: 'search' };

    this.handleSearch = this.handleSearch.bind(this);
    this.onMessage = this.onMessage.bind(this);
    this.requestRecommendations = this.requestRecommendations.bind(this);
    this.switchToSearch = this.switchToSearch.bind(this);
    this.switchToSuggestions = this.switchToSuggestions.bind(this);
  }

  public componentDidMount() {
    this.props.socket.addEventListener('message', this.onMessage);
    this.requestRecommendations();
  }

  public componentDidUpdate(prevProps: Props) {
    if (prevProps.socket !== this.props.socket) {
      this.props.socket.addEventListener('message', this.onMessage);
    }
  }

  public componentWillUnmount() {
    this.props.socket.removeEventListener('message', this.onMessage);
  }

  public render() {
    return (
      <div className="panel">
        <div className="panel-heading is-size-4">Add songs to your queue</div>
        <div className="panel-block">
          <div className="tabs is-fullwidth is-centered is-medium">
            <ul>
              <li className={(this.state.tab === 'search') ? 'is-active' : ''}>
                <a onClick={this.switchToSearch}>Search</a>
              </li>
              <li className={(this.state.tab === 'suggestions') ? 'is-active' : ''}>
                <a onClick={this.switchToSuggestions}>Suggestions</a>
              </li>
            </ul>
          </div>
        </div>
        {this.renderSearch()}
        {this.renderSuggestions()}
      </div>
    );
  }

  public renderSearch() {
    if (this.state.tab !== 'search') {
      return null;
    }

    return (
      <Search
        onSelect={this.props.onSelect}
        onSearch={this.handleSearch}
        results={this.state.searchResults}
      />
    );
  }

  public renderSuggestions() {
    if (this.state.tab !== 'suggestions') {
      return null;
    }

    if (typeof this.state.recommendations === 'undefined') {
      return (
        <div className="panel-block content">
          <p>Fetching suggestions&hellip;</p>
        </div>
      );
    }

    if (this.state.recommendations.length === 0) {
      return (
        <div className="panel-block content">
          <p>Unfortunately, we have no song suggestions for you at this time.</p>
        </div>
      );
    }

    const recommendationElements = this.state.recommendations.map((songDetails, index) => {
      const handler = () => this.props.onSelect(songDetails.id);

      return (
        <li key={`${songDetails.id}-${index}`}>
            <a onClick={handler} className="panel-block">
              <div className="media">
                <figure className="media-left">
                  <p className="image is-128x128">
                    <img src={songDetails.cover || unknownCoverImage} alt=""/>
                  </p>
                </figure>
                <div className="media-content">
                  <div className="content">
                    <p>
                      {songDetails.artists.join(', ')}
                      <br/>
                      <b>{songDetails.name}</b>
                    </p>
                  </div>
                </div>
              </div>
            </a>
        </li>
      );
    });

    return (
      <ul>
        {recommendationElements}
      </ul>
    );
  }

  private switchToSearch() {
    this.setState({ tab: 'search' });
  }

  private switchToSuggestions() {
    this.requestRecommendations();
    this.setState({ tab: 'suggestions' });
  }

  private onMessage(message: MessageEvent) {
    const response: WebSocketResponse = JSON.parse(message.data);

    if (isSearchResponse(response)) {
      this.setState({ searchResults: response.results });
    }

    if (isRecommendationResponse(response)) {
      this.setState({ recommendations: response.results });
    }
  }

  private handleSearch(query: string) {
    const data: SearchRequest = {
      query,
      type: 'search',
    };

    this.props.sendRequest(data);
    this.setState({ searchResults: undefined });

    ReactGA.event({
      action: 'Search',
      category: 'Guest',
      label: query,
    });

  }

  private requestRecommendations() {
    ReactGA.event({
      action: 'Request recommendations',
      category: 'Guest',
    });

    const seeds = (this.props.ownQueue.length > 0) ? this.props.ownQueue : this.props.partyQueue;
    if (seeds.length === 0) {
      this.setState({ recommendations: [] });

      return;
    }

    const recommendationRequest: RecommendationRequest = {
      seeds,
      type: 'recommendation',
    };

    // First set the recommendation state to loading, and only *then* fetch recommendations
    // (to avoid them coming in first, then setting it to loading again)
    this.setState({ recommendations: undefined }, () => this.props.sendRequest(recommendationRequest));
  }
}
