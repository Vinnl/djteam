import * as React from 'react';
import './Guest.css';

import { Slide, toast, ToastContainer, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import unknownCoverImage from '../images/unknown-cover.svg';

import * as ReactGA from '../analytics/GaDnt';
import {
  Party,
  QueueRequest,
  SongId,
  UserId,
  WebSocketRequest,
} from '../apiInterfaces';
import { AddPanel } from './AddPanel';

interface Props {
  party: Party;
  userId: UserId;
  socket: WebSocket;
  sendRequest: (request: WebSocketRequest) => void;
}
interface State { }

export class Guest extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.addToQueue = this.addToQueue.bind(this);
  }

  public render() {
    return (
      <>
        <ToastContainer
          position={'bottom-center' as ToastPosition.BOTTOM_CENTER}
          hideProgressBar={true}
          closeButton={false}
          transition={Slide}
        />
        {this.renderQueue()}
        <section className="section">
          <div className="container">
            <AddPanel
              onSelect={this.addToQueue}
              sendRequest={this.props.sendRequest}
              socket={this.props.socket}
              ownQueue={this.props.party.userQueues[this.props.userId]}
              partyQueue={this.props.party.queue}
            />
          </div>
        </section>
      </>
    );
  }

  public renderQueue() {
    const queue = this.props.party.userQueues[this.props.userId];
    if (queue.length === 0) {
      return null;
    }

    const queueElements = queue.map((songId, index) => { 
      const songDetails = this.props.party.trackDetails[songId];
      const classNames = (this.props.party.current === this.props.userId && index === 0)
        ? 'nowPlaying'
        : '';

      return (
        <li key={`${songId}-${index}`} className={`media ${classNames}`}>
          <figure className="media-left">
            <p className="image is-128x128">
              <img src={songDetails.cover || unknownCoverImage} alt=""/>
            </p>
          </figure>
          <div className="media-content">
            <div className="content">
              <p>
                {songDetails.artists.join(', ')}
                <br/>
                <b>{songDetails.name}</b>
              </p>
            </div>
          </div>
        </li>
      );
  });

    return (
      <section className="section">
        <div className="container">
          <h2 className="title">Your queue</h2>
          <ul className="guestQueue">
            {queueElements}
          </ul>
        </div>
      </section>
    );
  }

  public addToQueue(songId: SongId) {
    const data: QueueRequest = {
      songId,
      type: 'queue',
    };

    this.props.sendRequest(data);
    toast.info('Added to your queue!');

    // Apart from queueing the song, also ask for new recommendations:
    const queue = this.props.party.userQueues[this.props.userId];
    queue.push(songId);

    ReactGA.event({
      action: 'Queue a song',
      category: 'Guest',
      label: songId,
    });
  }
}
