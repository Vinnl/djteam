import * as React from 'react';

import { SearchResult, SongId } from '../apiInterfaces';
import unknownCoverImage from '../images/unknown-cover.svg';

interface Props {
  onSearch: (query: string) => void;
  onSelect: (songId: SongId) => void;
  results?: SearchResult[];
}
interface State {
  query?: string;
  form: FormValues;
}

interface FormValues {
  query: string;
}

export class Search extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { form: { query: '' } };

    this.onChangeQuery = this.onChangeQuery.bind(this);
    this.search = this.search.bind(this);
    this.getSelectHandler = this.getSelectHandler.bind(this);
  }

  public render() {
    return (
      <>
        <div className="panel-block">
          {this.renderForm()}
        </div>
        {this.renderResults()}
      </>
    );
  }

  public renderForm() {
    return (
      <form onSubmit={this.search}>
        <div className="field has-addons">
          <div className="control">
            <input
              type="search"
              onChange={this.onChangeQuery}
              autoFocus={true}
              placeholder="The Police - Roxanne"
              className="input is-medium"
              name="query"
            />
          </div>
          <div className="control">
            <input
              type="submit"
              className={`button is-medium ${ this.state.query && typeof this.props.results === 'undefined' ? 'is-loading' : '' }`}
              value="Search"
            />
          </div>
        </div>
      </form>
    );
  }

  public renderResults() {
    if (!this.state.query) {
      return null;
    }

    if (typeof this.props.results === 'undefined') {
      return <p className="panel-block">Loading...</p>;
    }

    if (this.props.results === null) {
      return <p className="panel-block">Error fetching results</p>;
    }

    if (this.props.results.length === 0) {
      return <p className="panel-block">No results</p>;
    }

    const resultElements = this.props.results.map((result, index) => (
      <li
        key={`${result.id}-${index}`}
      >
        <a onClick={this.getSelectHandler(result.id)} className="panel-block">
          <div className="media">
            <figure className="media-left">
              <p className="image">
                <img
                  src={result.cover || unknownCoverImage}
                  alt=""
                  className="image is-128x128"
                />
              </p>
            </figure>
            <div className="media-content">
              <p className="content">
                {result.artists.join(', ')}
                <br/>
                <b>{result.title}</b>
              </p>
            </div>
          </div>
        </a>
      </li>
    ));

    return <ul>{resultElements}</ul>;
  }

  public onChangeQuery(event: React.ChangeEvent<HTMLInputElement>) {
    const value = event.target.value;
    
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        query: value,
      },
    }));
  }

  public async search(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const query = this.state.form.query;

    // Store the query locally as well, so we know we're searching (and can show a loading screen)
    this.setState({ query }, () => {
      this.props.onSearch(query);
    });
  }

  public getSelectHandler(videoId: SongId) {
    return () => this.props.onSelect(videoId);
  }
}
