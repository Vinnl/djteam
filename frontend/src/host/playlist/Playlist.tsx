import * as React from 'react';
import './Playlist.css';

import { SongDetails, SongId, SpotifyAccessToken } from '../../apiInterfaces';
import unknownCoverImage from '../../images/unknown-cover.svg';
import { Player } from '../player/Player';

interface Props {
  current?: SongId;
  onNext: () => void,
  queue: SongDetails[];
  spotifyToken: SpotifyAccessToken
}

export const Playlist: React.SFC<Props> = (props) => {
  const queueElements = props.queue.map((songDetails, index) => {
    const isNowPlaying = (songDetails.id === props.current);
    const classNames = isNowPlaying ? 'nowPlaying' : '';

    return (
      <li key={`${songDetails.id}-${index}`} className={`media ${classNames}`}>
        <figure className="media-left">
          <p className="image is-128x128">
            <img src={songDetails.cover || unknownCoverImage} alt=""/>
          </p>
        </figure>
        <div className="media-content">
          <div className="content">
            <p>
              {songDetails.artists.join(', ')}
              <br/>
              <b>{songDetails.name}</b>
            </p>
          </div>
        </div>
      </li>
    );
  });

  return (
    <>
      <Player songId={props.current} onEnd={props.onNext} spotifyToken={props.spotifyToken}/>
      <ul className="Playlist">
        {queueElements}
      </ul>
    </>
  );
};
