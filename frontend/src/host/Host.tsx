import * as React from 'react';

import unknownCoverImage from '../images/unknown-cover.svg';
import * as ReactGA from '../analytics/GaDnt';
import { NextRequest, Party, SongId, SpotifyAccessToken, WebSocketRequest, PlaylistDetails, PlaylistId, QueuePlaylistRequest } from '../apiInterfaces';
import { Playlist } from './playlist/Playlist';

interface Props {
  party: Party;
  sendRequest: (request: WebSocketRequest) => void;
  spotifyToken: SpotifyAccessToken;
  spotifyPlaylists: PlaylistDetails[];
}
interface State {
  current?: SongId;
  keepaliveTimeout?: number;
}

export class Host extends React.Component<Props, State> {
  public static getDerivedStateFromProps(props: Props, state: State) {
    // When a song has ended, we tell the server we will switch to the next song.
    // The server can then respond with an empty queue, after which we reset the "now playing" state to `undefined.
    if (props.party.queue.length === 0) {
      return { current: undefined };
    }

    // If we're not currently playing anything, and the server tells us there is now a song in the queue,
    // start playing that song.
    if (typeof state.current === 'undefined' && props.party.queue.length > 0) {
      ReactGA.event({
        action: 'next',
        category: 'Host',
        label: props.party.queue[0],
      })

      return { current: props.party.queue[0] };
    }

    return null;
  }

  constructor(props: Props) {
    super(props);

    this.state = {};

    this.onNext = this.onNext.bind(this);
  }

  public componentDidMount() {
    // Set up a regular ping interval to prevent the Heroku dyno from sleeping while we're hosting a party.
    // See https://gitlab.com/Vinnl/djteam/issues/13

    // Dyno's sleep after 30 minutes of inactivity, so ping before that
    const pingIntervalDuration = (30 - 5) * 60 * 1000; // 25 minutes * 60 seconds * 1000 milliseconds
    const timeout = window.setInterval(ping, pingIntervalDuration);
    this.setState({ keepaliveTimeout: timeout })

    // Also send an initial ping to keep it alive for 30 minutes now,
    // since we're only hitting it with WebSocket requests that do not keep it alive:
    ping();
  }

  public componentWillUnmount() {
    if (typeof this.state.keepaliveTimeout !== 'undefined') {
      clearInterval(this.state.keepaliveTimeout);
    }
  }

  public render() {
    return (
      <>
        <section className="section">
          <div className="container">
            <div className="media">
              <p className="media-left">
                <b className="title">{this.props.party.partyId}</b>
              </p>
              <div className="media-content-content">
                <p>Additional DJ's can join using this code.</p>
              </div>
            </div>
          </div>
        </section>
        <section className="section">
          <div className="container">
            { (this.props.party.queue.length > 0) ? <h2 className="title">Queue</h2> : null }
            <Playlist
              current={this.state.current}
              queue={this.props.party.queue.map(songId => this.props.party.trackDetails[songId])}
              onNext={this.onNext}
              spotifyToken={this.props.spotifyToken}
            />
          </div>
        </section>
        {this.renderSpotifyPlaylists()}
      </>
    );
  }

  public renderSpotifyPlaylists() {
    if (this.props.spotifyPlaylists.length === 0) {
      return null;
    }

    const playlists = this.props.spotifyPlaylists.map(playlist => {
      return (
        <li key={playlist.id}>
          <a onClick={() => this.playPlaylist(playlist.id)}>
            <span className="media">
              <figure className="media-left">
                <p className="image is-128x128">
                  <img src={playlist.cover || unknownCoverImage} alt=""/>
                </p>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <b>{playlist.name}</b> ({playlist.numberOfSongs} tracks)
                  </p>
                </div>
              </div>
            </span>
          </a>
        </li>
      );
    });

    return (
      <section className="section">
        <div className="container">
          <h2 className="title">Play one of your playlists</h2>
          <ul>
            {playlists}
          </ul>
        </div>
      </section>
    );
  }

  public onNext() {
    if (typeof this.state.current === 'undefined') {
      return;
    }

    const data: NextRequest = {
      partyId: this.props.party.partyId,
      prevSongId: this.state.current,
      songId: this.props.party.queue[1],
      type: 'next',
    };

    this.setState({ current: data.songId }, () => {
      this.props.sendRequest(data);
    });

    ReactGA.event({
      action: 'next',
      category: 'Host',
      label: data.songId,
    })
  }

  public playPlaylist(playlistId: PlaylistId) {
    const data: QueuePlaylistRequest = {
      type: 'queuePlaylist',
      partyId: this.props.party.partyId,
      playlistId,
    };

    this.props.sendRequest(data);

    ReactGA.event({
      action: 'queuePlaylist',
      category: 'Host',
      label: playlistId,
    })
  }
}

function ping() {
  const url = process.env.REACT_APP_BACKEND_HTTP_URL || `https://${document.location.host}`;
  fetch(`${url}/ping`);
}
