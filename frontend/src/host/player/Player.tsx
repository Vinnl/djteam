import * as React from 'react';

import { SongId, SpotifyAccessToken } from '../../apiInterfaces';
import { SpotifyPlayer } from '../../spotify/SpotifyPlayer';

interface Props {
  onEnd: () => void,
  songId?: SongId;
  spotifyToken: SpotifyAccessToken;
}

export const Player: React.SFC<Props> = (props) => {
  return (
    <SpotifyPlayer
      songId={props.songId}
      accessToken={props.spotifyToken}
      name="DJ Team"
      onEnd={props.onEnd}
      />
  );
};
