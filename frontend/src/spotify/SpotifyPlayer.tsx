import * as React from 'react';

import { SongId, SpotifyAccessToken } from '../apiInterfaces';

// See https://developer.spotify.com/documentation/web-playback-sdk/reference/#playing-a-spotify-uri
type GetOAuthToken = (callback: (token: string) => void) => void;
interface PlayerInstance {
  _options: {
    getOAuthToken: GetOAuthToken;
    id: string;
  };
  removeListener: (eventName: string) => void;
}

// See https://developer.spotify.com/documentation/web-playback-sdk/reference/#object-web-playback-state
interface SpotifyWebPlaybackState {
  paused: boolean;
  position: number;
  track_window: {
    current_track: {
      id: string;
      uri: string;
      type: 'track';
    };
  };
}

interface Props {
  accessToken: SpotifyAccessToken;
  name: string;
  onEnd: () => void;
  songId?: SongId;
}

interface State {
  deviceId?: string;
  player?: PlayerInstance;
  // The following variable is the unfortunate result of having to manually keep the app
  // state in sync with the (imperatively controlled) Spotify Player's state,
  // which appears to throw the `ready` event multiple times
  // (which results in the same song getting started multiple times concurrently).
  songAddedToPlayer?: SongId;
}

export class SpotifyPlayer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {};
  }

  public componentDidMount() {
    this.state = {};

    // If we render the <script> tag including the Spotify player,
    // it won't actually get fetched. Therefore, just dynamically
    // add it to the rendered element.
    const scriptTag = document.createElement('script');
    scriptTag.setAttribute('src', 'https://sdk.scdn.co/spotify-player.js');
    const playerContainer = document.getElementById('spotifyPlayer');
    if (playerContainer !== null) {
      playerContainer.appendChild(scriptTag);
    }

    const getOAuthToken: GetOAuthToken = (callback: (token: string) => void) => { callback(this.props.accessToken); };
    (window as any).onSpotifyWebPlaybackSDKReady = () => {
      const player = new (window as any).Spotify.Player({
        getOAuthToken,
        name: this.props.name,
      });
    
      // Error handling
      // player.addListener('initialization_error', ({ message }) => { console.error(message); });
      // player.addListener('authentication_error', ({ message }) => { console.error(message); });
      // player.addListener('account_error', ({ message }) => { console.error(message); });
      // player.addListener('playback_error', ({ message }) => { console.error(message); });
    
      // Playback status updates
      player.addListener('player_state_changed', (state: SpotifyWebPlaybackState) => {
        if (state.paused && state.position === 0 && state.track_window.current_track.id === this.props.songId) {
          this.props.onEnd();
        }
      });
    
      // Ready
      player.addListener('ready', ({ device_id }: { device_id: string }) => {
        this.setState({ deviceId: device_id, player });

        if (typeof this.props.songId !== 'undefined' && typeof this.state.songAddedToPlayer === 'undefined') {
          this.play(this.props.songId, player);
        }
      });
    
      // // Not Ready
      // player.addListener('not_ready', ({ device_id }) => {
      //   console.log('Device ID has gone offline', device_id);
      // });
    
      // Connect to the player!
      player.connect();
    };
    
  }

  public componentDidUpdate(prevProps: Props) {
    if (
      typeof this.props.songId !== 'undefined'
      && this.props.songId !== prevProps.songId
      && typeof this.state.player !== 'undefined'
    ) {
      this.play(this.props.songId, this.state.player);
    }
  }

  public componentWillUnmount() {
    if (typeof this.state.player !== 'undefined') {
      this.state.player.removeListener('ready');
      this.state.player.removeListener('player_state_changed');
    }
  }

  public render () {
    return <div id="spotifyPlayer"/>;
  }

  public play(songId: SongId, player: PlayerInstance) {
    this.setState({ songAddedToPlayer: songId }, () => {
      player._options.getOAuthToken((spotifyToken) => {
        fetch(`https://api.spotify.com/v1/me/player/play?device_id=${player._options.id}`, {
          body: JSON.stringify({ uris: [`spotify:track:${songId}`] }),
          headers: {
            'Authorization': `Bearer ${spotifyToken}`,
            'Content-Type': 'application/json',
          },
          method: 'PUT',
        });
      });
    });
  }
}
