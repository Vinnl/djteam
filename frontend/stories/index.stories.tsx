import * as React from 'react';

import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';

import { Player } from '../src/host/player/Player';

const stories = storiesOf('Player', module);

stories.add('With a video loaded', () => (
  <Player
    songId={text('Spotify track ID', '0kvMWkuRwzd8YX5s60pjdE')}
    onEnd={action('song-finished')}
    spotifyToken={text('Spotify access token', 'xxx')}
  />
));
