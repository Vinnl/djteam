const path = require("path");

module.exports = (baseConfig, env, config) => {
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    include: [
      path.resolve(__dirname, "../src"),
      path.resolve(__dirname, "../stories"),
    ],
    loader: require.resolve('awesome-typescript-loader'),
    options: { configFileName: path.resolve(__dirname, '../tsconfig.storybook.json') },
  });
  config.resolve.extensions.push(".ts", ".tsx");
  return config;
};
