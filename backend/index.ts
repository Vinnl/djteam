import WebSocket from 'ws';
import express from 'express';
import * as http from 'http';
import { config as loadEnvFile } from 'dotenv';

import {
  WebSocketRequest,
  isHostRequest,
  isJoinRequest,
  isQueueRequest,
  isNextRequest,
  isReconnectRequest,
  ConnectionResponse,
  ReconnectResponse,
  isSearchRequest,
  SearchResponse,
  PingRequest,
  isRecommendationRequest,
  RecommendationResponse,
  isQueuePlaylistRequest,
} from './apiInterfaces';
import {
  newUser,
  resetLeaveTimeout,
  adoptUser,
  startParty,
  joinParty,
  getPartyForUser,
  queueSong,
  nextSong,
  searchInParty,
  getPartyDetails,
  recommendForParty,
  queuePlaylist,
} from './data';

loadEnvFile();
 
let app;
if (typeof process.env.FRONTEND_PATH !== 'undefined') {
  app = express();
  app.use(express.static(process.env.FRONTEND_PATH));
}
const server = http.createServer(app);

server.addListener('request', (req: http.IncomingMessage, res: http.ServerResponse) => {
  if (req.url === '/ping') {
    res.end('pong');
  }
});

const port = typeof process.env.PORT === 'string' ? Number.parseInt(process.env.PORT, 10) : 5000;
const wss = new WebSocket.Server({ server });
server.listen(port, () => console.log('Server running on port', port));

wss.on('connection', function connection(ws) {
  let [userId, token] = newUser(ws);
  console.log('User connected:', userId);

  keepalive(ws);

  const connectionResponse: ConnectionResponse = {
    type: 'connection',
    token,
    userId,
  };
  ws.send(JSON.stringify(connectionResponse));

  ws.on('close', function close() {
    resetLeaveTimeout(userId);
  });

  ws.on('message', function incoming(message) {
    // TODO Change methods that accept a userID, to make them accept the token instead,
    //      so that we don't have to know the userID here.
    if (typeof message !== 'string') {
      console.log('WebSocket request is not a string');

      return;
    }

    const data: WebSocketRequest = JSON.parse(message);
    console.log('received: %s', message);

    if (isReconnectRequest(data)) {
      const newUserId = adoptUser(data.token, userId);

      if (newUserId === null) {
        // Should we send an error message to the user here?
        return;
      }

      userId = newUserId;
      token = data.token;

      const partyId = getPartyForUser(userId);
      const party = (partyId !== null) ? getPartyDetails(partyId) : null;

      const message: ReconnectResponse = {
        type: 'reconnect',
        userId,
        party: party || undefined,
      };

      ws.send(JSON.stringify(message));
    }

    if (isHostRequest(data)) {
      startParty(userId, data.spotifyAuth);
    }

    if (isQueuePlaylistRequest(data)) {
      queuePlaylist(data.partyId, data.playlistId);
    }

    if (isJoinRequest(data)) {
      joinParty(userId, data.partyId);
    }

    if (isRecommendationRequest(data)) {
      const partyId = getPartyForUser(userId);

      if (partyId === null) {
        return;
      }

      recommendForParty(partyId, data.seeds)
      .then(results => {
        const message: RecommendationResponse = {
          type: 'recommendation',
          results,
        };

        ws.send(JSON.stringify(message));
      });
    }

    if (isSearchRequest(data)) {
      const partyId = getPartyForUser(userId);

      if (partyId === null) {
        return;
      }

      searchInParty(partyId, data.query)
      .then((results) => {
        const message: SearchResponse = {
          results,
          type: 'search',
        };

        ws.send(JSON.stringify(message));
      });
    }

    if (isQueueRequest(data)) {
      if (typeof data.songId !== 'string') {
        console.log('Invalid song ID passed');

        return;
      }

      queueSong(userId, data.songId);
    }

    if (isNextRequest(data)) {
      nextSong(token);
    }
  });
});

const keepalive: (socket: WebSocket) => void = (socket) => {
  // Connections close after 55 seconds of inactivity,
  // so send at least one request every <55 (i.e. 50) seconds
  const intervalDuration = 50 * 1000;

  const timer = setInterval(
    () => {
      if (socket.readyState === WebSocket.OPEN) {
        const request: PingRequest = { type: 'ping' };
        socket.send(JSON.stringify(request));
      } else {
        clearInterval(timer);
      }
    },
    intervalDuration,
  );
}
