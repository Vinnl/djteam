import WebSocket from 'ws';
import { v4 as uuid } from 'uuid';

import {
  Party,
  PartyId,
  Queue,
  UserId,
  Token,
  SongId,
  SpotifyAuthCode,
  SpotifyAccessToken,
  WebSocketResponse,
  HostResponse,
  EndResponse,
  PartyUpdatedResponse,
  TokenRefreshResponse,
  SearchResult,
  SongDetails,
  PlaylistDetails,
  PlaylistId,
} from "./apiInterfaces";
import { getSpotifyTokens, refreshAccessToken, fetchSpotify, getRecommendations, getProfile, getPlaylists, getPlaylist } from './fetchSpotify';

const parties: Party[] = [];
interface User {
  socket: WebSocket;
  queue: Queue;
  tokens: string[];
  partyId?: PartyId;
  cleanupTimer?: NodeJS.Timer;
}
interface SpotifyData { accessToken: SpotifyAccessToken; refreshToken: string; refreshTimeout: NodeJS.Timer; }
const spotifyTokens: { [ partyId: string ]: SpotifyData } = {};
const users: Array<User> = [];
const connections: { [token: string] : UserId } = {};

// Store track details per party, so we can clean them up when a party ends, rather than keeping them in memory:
const tracksDetailsByParty: { [ partyId: number ]: { [ songId: string ]: SongDetails } } = {};

function sendMessageToUser(userId: UserId, message: WebSocketResponse) {
  if (typeof users[userId] === 'undefined') {
    console.error('Trying to send a WebSocket message to a non-existent user');

    return;
  }

  if (users[userId].socket.readyState !== WebSocket.OPEN) {
    return;
  }

  users[userId].socket.send(JSON.stringify(message));
}

function sendMessageToParticipants(party: Party, message: WebSocketResponse) {
  sendMessageToUser(party.host, message);
  party.guests.forEach(userId => sendMessageToUser(userId, message));
}

export async function startParty(
  hostUserId: UserId,
  spotifyAuth: {
    code: SpotifyAuthCode,
    redirectUri: string,
  },
): Promise<void> {
  if (typeof users[hostUserId] === 'undefined') {
    console.error('Trying to start a party as a non-existent user');

    return;
  }

  const tokenData = await getSpotifyTokens(spotifyAuth.code, spotifyAuth.redirectUri);

  // Refresh the token five minutes before it runs out
  const timer = setTimeout(() => refreshPartyAccessToken(partyId), tokenData.expiresIn - 5 * 60 * 1000);

  const partyId = hostUserId;
  parties[partyId] = {
    host: hostUserId,
    guests: [],
    queue: [],
    userQueues: {},
    trackDetails: {},
    partyId,
  };
  users[hostUserId].partyId = partyId;

  spotifyTokens[partyId] = {
    accessToken: tokenData.accessToken,
    refreshToken: tokenData.refreshToken,
    refreshTimeout: timer,
  };

  const profile = await getProfile(tokenData.accessToken);
  const playlistResults = await getPlaylists(profile.id, tokenData.accessToken);
  const playlists = playlistResults.items.map<PlaylistDetails>(result => {
    return {
      id: result.id,
      name: result.name,
      numberOfSongs: result.tracks.total,
      cover: (result.images.length > 0) ? result.images[0].url : undefined,
    };
  });

  const response: HostResponse = {
    type: 'host',
    spotifyAccessToken: tokenData.accessToken,
    party: parties[partyId],
    playlists,
  };
  sendMessageToUser(hostUserId, response);
}

export function endParty(partyId: PartyId): void {
  if (typeof parties[partyId] === 'undefined') {
    return;
  }

  const party = parties[partyId];

  party.guests.forEach((userId) => delete users[userId].partyId);
  delete users[party.host].partyId;

  const data: EndResponse = {
    type: 'end',
    partyId,
  };
  sendMessageToParticipants(party, data);

  delete parties[partyId];
  if (typeof spotifyTokens[partyId] !== 'undefined') {
    clearTimeout(spotifyTokens[partyId].refreshTimeout);
  }
  delete tracksDetailsByParty[partyId];
  delete spotifyTokens[partyId];
}

export function getPartyDetails(partyId: PartyId): Party | null {
  if (typeof parties[partyId] === 'undefined') {
    return null;
  }

  return parties[partyId];
}

async function refreshPartyAccessToken(partyId: PartyId) {
  if (typeof spotifyTokens[partyId] === 'undefined') {
    return;
  }

  const tokenData = await refreshAccessToken(spotifyTokens[partyId].refreshToken);

  // Make sure the party hasn't ended while we were awaiting a new token:
  if (typeof spotifyTokens[partyId] === 'undefined') {
    return;
  }

  // Refresh the token five minutes before it runs out
  const timer = setTimeout(() => refreshPartyAccessToken(partyId), tokenData.expiresIn - 5 * 60 * 1000)

  spotifyTokens[partyId].accessToken = tokenData.accessToken;
  spotifyTokens[partyId].refreshTimeout = timer;

  const message: TokenRefreshResponse = {
    spotifyAccessToken: tokenData.accessToken,
    type: 'tokenRefresh',
  };

  users[parties[partyId].host].socket.send(JSON.stringify(message));
};

export function getAccessTokenForParty(partyId: PartyId): SpotifyAccessToken | null {
  if (typeof spotifyTokens[partyId] === 'undefined') {
    return null;
  }

  return spotifyTokens[partyId].accessToken;
}

function getTrackDetailsForSongs(partyId: PartyId, songs: SongId[]): { [songId: string]: SongDetails; } {
  return songs.reduce(
    (songDetails, songId) => {
      songDetails[songId] = tracksDetailsByParty[partyId][songId];

      return songDetails;
    },
    {} as { [songId: string]: SongDetails });
}

export function getPartyForUser(userId: UserId): PartyId | null {
  if (typeof users[userId] === 'undefined') {
    return null;
  }
  const user = users[userId];

  if (typeof user.partyId === 'undefined') {
    return null;
  }

  return user.partyId;
}

export function joinParty(userId: UserId, partyId: PartyId) {
  if (typeof users[userId] === 'undefined') {
    console.error('Trying to join a party as a non-existent user');

    return;
  }

  users[userId].partyId = partyId;
  updateParty(partyId, (prevParty: Party) => {
    const newGuestList = prevParty.guests.slice();
    newGuestList.push(userId);

    return {
      ...prevParty,
      guests: newGuestList,
    };
  });
}

export function leaveParty(userId: UserId, partyId: PartyId) {
  if (typeof parties[partyId] === 'undefined') {
    console.error('Trying to leave a non-existent party');

    return;
  }

  if (parties[partyId].host === userId) {
    endParty(partyId);

    return;
  }

  updateParty(partyId, (prevParty: Party) => {
    const userGuestIndex = prevParty.guests.indexOf(userId);

    if (userGuestIndex === -1) {
      return prevParty;
    }

    const newGuestList =
      prevParty.guests.slice(0, userGuestIndex)
      .concat(prevParty.guests.slice(userGuestIndex + 1));

    return {
      ...prevParty,
      guests: newGuestList,
    };
  });
}

function getPartyIdForHost(connection: Token): PartyId {
  if (typeof connections[connection] === 'undefined') {
    return -1;
  }
  const userId = connections[connection];

  if (typeof users[userId] === 'undefined') {
    return -1;
  }
  const user = users[userId];

  if (typeof user.partyId === 'undefined' || typeof parties[user.partyId] === 'undefined') {
    return -1;
  }
  const party = parties[user.partyId];

  if (party.host !== userId) {
    return -1;
  }

  return user.partyId;
}

function recomputeParty(party: Party, allUsers: User[]): Party {
  const queue = computeQueue(party, allUsers);

  const updatedParty: Party = {
    ...party,
    queue,
    userQueues: party.guests.reduce(
      (soFar, userId) => {
        soFar[userId] = allUsers[userId].queue;

        return soFar;
      },
      {} as { [ userId: number ]: Queue },
    ),
    trackDetails: getTrackDetailsForSongs(party.partyId, queue),
  };

  return updatedParty;
}

function updateQueue(userId: UserId, updater: (prevQueue: Queue) => Queue): void {
  if (typeof users[userId] === 'undefined') {
    return;
  }
  const user = users[userId];

  const newQueue = updater(user.queue);
  if (newQueue === user.queue) {
    // Queue hasn't changed
    return;
  }
  user.queue = newQueue;

  if (typeof user.partyId !== 'undefined') {
    syncParty(user.partyId);
    updateParty(user.partyId, (prevParty) => {
      if (typeof prevParty.current !== 'undefined' || user.queue.length === 0) {
        return prevParty;
      }

      return {
        ...prevParty,
        current: userId,
      };
    });
  }
}

function updateParty(partyId: PartyId, updater: (prevParty: Party) => Party): void {
  if (typeof parties[partyId] === 'undefined') {
    return;
  }
  const newParty = updater(parties[partyId]);
  if (newParty === parties[partyId]) {
    // Party hasn't changed
    return;
  }

  syncParty(partyId, newParty);
}

function syncParty(partyId: PartyId, newParty?: Party): void {
  if (typeof parties[partyId] === 'undefined') {
    return;
  }

  // We want to recompute the party sometimes even if the party itself hasn't changed,
  // for example when a user adds a song to their queue:
  newParty = newParty || parties[partyId];

  parties[partyId] = recomputeParty(newParty, users);

  const message: Partial<PartyUpdatedResponse> = {
    party: parties[partyId],
    type: 'partyUpdate',
  };
  sendMessageToUser(parties[partyId].host, { ...message, role: 'host' } as PartyUpdatedResponse);
  parties[partyId].guests.forEach(userId => sendMessageToUser(userId, { ...message, role: 'participant' } as PartyUpdatedResponse));
}

export function getQueueOfUsers(hostId: UserId, guestList: UserId[], currentUser?: UserId): UserId[] {
  const participatingUsers = guestList.slice();
  participatingUsers.push(hostId);
  if (typeof currentUser === 'undefined') {
    return participatingUsers;
  }

  const currentGuestIndex = participatingUsers.findIndex((userId) => userId === currentUser);

  return participatingUsers.slice(currentGuestIndex).concat(participatingUsers.slice(0, currentGuestIndex));
}

export function computeQueue(party: Party, allUsers: User[]): Queue {
  const queueOfUsers = getQueueOfUsers(party.host, party.guests, party.current);

  // Copy (through `.slice()`) of every user's queue, in the correct order:
  let remainingQueues: Queue[] = queueOfUsers
    .filter(userId => typeof allUsers[userId] !== 'undefined')
    .map(userId => allUsers[userId].queue.slice());
  // This will be an array of the queued songs in the correct order
  const partyQueue: Queue = [];
  do {
    // Get a copy of every user's queue, filtering out empty queues.
    // This way, we'll only keep adding songs to the final queue if guests have more songs queued.
    remainingQueues = remainingQueues
      .filter(queue => Array.isArray(queue) && queue.length > 0);

    remainingQueues.forEach(userQueue => {
      // We need the typecast here - TypeScript thinks `.shift()` can return `undefined`,
      // but since we filtered out arrays with length 0, it will always return a SongId.
      partyQueue.push(userQueue.shift() as SongId);
    });
  } while (remainingQueues.length > 0)

  return partyQueue;
}

export async function recommendForParty(partyId: PartyId, seeds: SongId[]): Promise<SongDetails[]> {
  const spotifyToken = getAccessTokenForParty(partyId);
  if (spotifyToken === null) {
    throw new Error('Could not obtain the access token for a party on behalf of which to request recommendations');
  }

  const results = await getRecommendations(seeds, spotifyToken);
  tracksDetailsByParty[partyId] = tracksDetailsByParty[partyId] || {};
  results.tracks.forEach((result) => {
    tracksDetailsByParty[partyId][result.id] = {
      id: result.id,
      artists: result.artists.map(artist => artist.name),
      name: result.name,
      durationMs: result.duration_ms,
      cover: (result.album.images.length > 0) ? result.album.images[0].url : undefined,
    };
  });

  const recommendedTracks: SongDetails[] = results.tracks.map<SongDetails>((result) => ({
    id: result.id,
    artists: result.artists.map(artist => artist.name),
    cover: result.album.images[0].url,
    durationMs: result.duration_ms,
    name: result.name,
  }));

  return recommendedTracks;
}

export async function searchInParty(partyId: PartyId, query: string): Promise<SearchResult[]> {
  const spotifyToken = getAccessTokenForParty(partyId);
  if (spotifyToken === null) {
    throw new Error('Could not obtain the access token for a party on behalf of which to search for a song');
  }

  const results = await fetchSpotify(query, spotifyToken);
  tracksDetailsByParty[partyId] = tracksDetailsByParty[partyId] || {};
  results.forEach((result) => {
    tracksDetailsByParty[partyId][result.id] = {
      id: result.id,
      artists: result.artists,
      name: result.title,
      durationMs: result.durationMs,
      cover: result.cover,
    };
  });

  return results;
}

export function nextSong(connection: Token) {
  const partyId = getPartyIdForHost(connection);

  if (typeof parties[partyId] === 'undefined') {
    console.error('A non-host is trying to skip to the next song');

    return;
  }
  const party = parties[partyId];

  // Get the user whose queue the next song will come from
  const queueOfUsersWithQueuedSongs = getQueueOfUsers(party.host, party.guests, party.current)
    .filter((userId) => users[userId].queue.length > 0);
  const nextUser = (typeof party.current !== 'undefined' && queueOfUsersWithQueuedSongs.length > 1)
    // If there is someone responsible for the current song,
    // and there are other people with songs queued,
    // the next song should be of another guest:
    ? queueOfUsersWithQueuedSongs[1]
    : (queueOfUsersWithQueuedSongs.length > 0 && users[queueOfUsersWithQueuedSongs[0]].queue.length > 1)
      // If the person responsible for the current song is the only one with queued songs,
      // and they have additional songs in their queue, they can have another turn:
      ? party.current
      // If none of the guests have any more songs queued, we're done
      : undefined;

  // Remove the song that just finished, if any, from its submitter's queue
  if (typeof party.current !== 'undefined') {
    users[party.current].queue.shift();
  }

  // Recompute the party
  updateParty(partyId, (prevParty: Party) => ({
    ...prevParty,
    current: nextUser,
  }))
}

export async function queuePlaylist(partyId: PartyId, playlistId: PlaylistId) {
  const spotifyToken = getAccessTokenForParty(partyId);
  if (spotifyToken === null) {
    throw new Error('Could not obtain the access token for a party on behalf of which to queue a playlist');
  }

  const results = await getPlaylist(playlistId, spotifyToken);

  tracksDetailsByParty[partyId] = tracksDetailsByParty[partyId] || {};
  results.tracks.items.forEach((item) => {
    const track = item.track;
    tracksDetailsByParty[partyId][track.id] = {
      id: track.id,
      artists: track.artists.map(artist => artist.name),
      name: track.name,
      durationMs: track.duration_ms,
      cover: (track.album.images.length > 0) ? track.album.images[0].url : undefined,
    };
  });

  const party = getPartyDetails(partyId);

  if (party === null) {
    throw new Error('Could not find the host for whom to queue a playlist');
  }

  updateQueue(party.host, () => {
    return results.tracks.items.map(track => track.track.id);
  });
}

export function queueSong(userId: UserId, songId: SongId) {
  updateQueue(userId, (prevQueue) => {
    const newQueue = prevQueue.slice();
    newQueue.push(songId);

    return newQueue;
  });
}

export function newUser(socket: WebSocket): [UserId, Token] {
  const token: Token = uuid();

  const user: User = {
    socket,
    queue: [],
    tokens: [ token ],
  };

  const id: UserId = users.push(user) - 1;
  connections[token] = id;

  return [id, token];
}

export function adoptUser(connection: Token, redundantUserId: UserId): UserId | null {
  if (typeof connections[connection] === 'undefined') {
    console.error('Trying to adopt a user with a non-existent token');

    return null;
  }
  const userIdToAdopt = connections[connection];

  if (typeof users[userIdToAdopt] === 'undefined') {
    console.error('Trying to adopt a non-existent user');

    return null;
  }
  const userToAdopt = users[userIdToAdopt];

  if (typeof users[redundantUserId] === 'undefined') {
    console.error('A non-existent user is trying to adopt another one');

    return null;
  }
  const redundantUser = users[redundantUserId];

  // Transfer the WebSocket connection
  userToAdopt.socket = redundantUser.socket;

  // Remove the redundant user
  removeUser(redundantUserId);

  // Reset leave timeout for the user we adopted
  resetLeaveTimeout(userIdToAdopt);

  return userIdToAdopt;
}

export function removeUser(id: UserId): void {
  if (typeof users[id] === 'undefined') {
    console.error('Trying to remove a non-existent user');

    return;
  }

  const user = users[id];

  // If this user is part of a party, remove them from it
  if (typeof user.partyId !== 'undefined') {
    leaveParty(id, user.partyId);
  }

  if (typeof user.cleanupTimer !== 'undefined') {
    clearTimeout(user.cleanupTimer);
  }

  user.tokens.forEach(token => delete connections[token]);
  delete users[id];
}

export function resetLeaveTimeout(userId: UserId) {
  if (typeof users[userId] === 'undefined') {
    console.error('Trying to reset the leave timeout of a non-existent user');

    return;
  }
  const user = users[userId];

  if (typeof user.cleanupTimer !== 'undefined') {
    clearTimeout(user.cleanupTimer);
  }

  // Remove this user after one hour of inactivity
  user.cleanupTimer = setTimeout(() => removeUser(userId), 1 * 60 * 60 * 1000);
}

function isValid<ValidResult> (param: ValidResult | null): param is ValidResult {
  return param !== null;
}
