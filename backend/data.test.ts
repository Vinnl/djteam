import { getQueueOfUsers, computeQueue } from './data';
import { PartyId, Party } from './apiInterfaces';

const mockUser = { queue: [], socket: {} as any, tokens: [] };
const mockParty: Party = {
  current: 1,
  guests: [1, 2, 3],
  host: 0,
  queue: [],
  userQueues: {},
  partyId: 0,
  trackDetails: {},
};
const mockUsers = [
  { ...mockUser, queue: [] },
  { ...mockUser, queue: [ 'Song 1 for user 1' ] },
  { ...mockUser, queue: [ 'Song 1 for user 2', 'Song 2 for user 2' ] },
];
// mockUsers[3] intentionally left empty
mockUsers[4] = { ...mockUser, queue: [ 'Song 1 for user 4' ] };

describe('getQueueOfUsers', () => {
  it ('should preserve the order of guests, but start with the current DJ', () => {
    expect(getQueueOfUsers(0, [ 1, 3, 5, 7 ], 3))
    .toEqual([ 3, 5, 7, 1 ]);
  });
});

describe('computeQueue', () => {
  it('should add all songs by all guests, in the correct order', () => {
    expect(computeQueue(mockParty, mockUsers))
    .toEqual([ 'Song 1 for user 1', 'Song 1 for user 2', 'Song 2 for user 2' ]);
  });
});
