import fetch from 'node-fetch';

import { SpotifyAccessToken, SearchResult, SongId, SpotifyAuthCode, PlaylistId } from './apiInterfaces';

interface SpotifyErrorResponse {
 error: string;
 error_description: string;
}

export const fetchSpotify: (query: string, token: SpotifyAccessToken) => Promise<SearchResult[]> =
  async (query, token) => {
    const searchUrl = `https://api.spotify.com/v1/search?q=${encodeURIComponent(query)}&type=track&limit=10`;
    const response = await fetch(searchUrl, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const data: SpotifySearchResults = await response.json();

    const results: SearchResult[] = data.tracks.items.map((item) => {
      const result: SearchResult = {
        durationMs: item.duration_ms,
        id: item.id,
        title: item.name,
        artists: item.artists.map(artist => artist.name),
        cover: (item.album.images.length > 0) ? item.album.images[0].url : undefined,
      };

      return result;
    });

    return results;
  };

interface SpotifySearchResults {
  tracks: {
    items: Array<{
      id: SongId,
      name: string;
      duration_ms: number;
      type: 'track';
      album: {
        images: Array<{
          height: number | null;
          width: number | null;
          url: string;
        }>;
      };
      artists: Array<{
        name: string;
        type: 'artist';
        id: string;
      }>;
    }>;
  };
}

interface SpotifyTokenResponse {
  access_token: string;
  token_type: 'Bearer';
  scope: string;
  expires_in: number;
  refresh_token: string;
}
export const getSpotifyTokens: (code: SpotifyAuthCode, redirectUri: string) => Promise<
  { accessToken: SpotifyAccessToken, refreshToken: string, expiresIn: number }
> = async (code, redirectUri) => {
  const authorization =
    new Buffer(`${process.env.SPOTIFY_CLIENT_ID}:${process.env.SPOTIFY_CLIENT_SECRET}`)
    .toString('base64');

  const response = await fetch('https://accounts.spotify.com/api/token', {
    body: `code=${code}&grant_type=authorization_code&redirect_uri=${redirectUri}`,
    method: 'POST',
    headers: {
      'Authorization': `Basic ${authorization}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });

  const data: SpotifyTokenResponse | SpotifyErrorResponse = await response.json();
  if (!isSuccessfulSpotifyResponse(data)) {
    throw new Error(data.error_description);
  }

  return {
    accessToken: data.access_token,
    expiresIn: data.expires_in * 1000,
    refreshToken: data.refresh_token,
  };
};

interface TokenRefreshResponse {
  access_token: string;
  expires_in: number;
}
export const refreshAccessToken: (refreshToken: string) => Promise<{
  accessToken: SpotifyAccessToken,
  expiresIn: number,
}> = async (refreshToken) => {
  const authorization =
    new Buffer(`${process.env.SPOTIFY_CLIENT_ID}:${process.env.SPOTIFY_CLIENT_SECRET}`)
    .toString('base64');
  const response = await fetch('https://accounts.spotify.com/api/token', {
    body: `grant_type=refresh_token&refresh_token=${refreshToken}`,
    headers: {
      'Authorization': `Basic ${authorization}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    method: 'POST',
  });

  const data: TokenRefreshResponse | SpotifyErrorResponse = await response.json();
  if (!isSuccessfulSpotifyResponse(data)) {
    throw new Error(data.error_description);
  }

  return {
    accessToken: data.access_token,
    expiresIn: data.expires_in * 1000,
  };
}

interface RecommendationResponse {
  tracks: Array<{
    type: 'track';
    id: SongId;
    is_playable: boolean;
    name: string;
    duration_ms: number;
    artists: Array<{
      name: string;
      type: 'artist';
    }>;
    album: {
      images: Array<{
        height: number | null;
        width: number | null;
        url: string;
      }>;
    };
  }>;
  seeds: Array<{
    id: SongId;
    type: 'track' | 'artist';
  }>;
}
export const getRecommendations: (
  seedTracks: SongId[],
  token: SpotifyAccessToken,
) => Promise<RecommendationResponse> = async (seedTracks, token) => {
  // Spotify only takes up to five seeds, so take the most recently-added five songs
  const seeds = seedTracks.slice(-5).join(',');

  const searchUrl = `https://api.spotify.com/v1/recommendations?seed_tracks=${encodeURIComponent(seeds)}&type=track`;
  const response = await fetch(searchUrl, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data: RecommendationResponse = await response.json();

  return data;
};

interface ProfileResponse {
  display_name: string;
  id: string;
  image: Array<{
    url: string;
    height: number;
    width: number;
  }>;
  type: 'user';
}
export const getProfile: (
  token: SpotifyAccessToken,
) => Promise<ProfileResponse> = async (token) => {
  const profileUrl = 'https://api.spotify.com/v1/me';
  const response = await fetch(profileUrl, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data: ProfileResponse = await response.json();

  return data;
};

interface PlaylistResponse {
  tracks: {
    items: Array<{
      track: {
        id: SongId;
        duration_ms: number;
        album: {
          images: Array<{
            url: string;
            width: number;
            height: number;
          }>;
          name: string;
        };
        name: string;
        artists: Array<{
          name: string;
          type: 'artist';
        }>;
      };
    }>;
  };
  type: 'playlist';
}
export const getPlaylist: (
  playlistId: PlaylistId,
  token: SpotifyAccessToken,
) => Promise<PlaylistResponse> = async (playlistId, token) => {
  const playlistUrl = `https://api.spotify.com/v1/playlists/${playlistId}`;
  const response = await fetch(playlistUrl, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data: PlaylistResponse = await response.json();

  return data;
};

interface PlaylistListResponse {
  items: Array<{
    href: string;
    id: string;
    images: Array<{
      url: string;
      height: number;
      width: number;
    }>;
    tracks: {
      href: string;
      total: number;
    };
    name: string;
    type: 'playlist';
  }>;
}
export const getPlaylists: (
  spotifyUserId: string,
  token: SpotifyAccessToken,
) => Promise<PlaylistListResponse> = async (spotifyUserId, token) => {
  const playlistsUrl = `https://api.spotify.com/v1/users/${spotifyUserId}/playlists`;
  const response = await fetch(playlistsUrl, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data: PlaylistListResponse = await response.json();

  return data;
};

function isSuccessfulSpotifyResponse<SuccessResponse> (response: SuccessResponse | SpotifyErrorResponse): response is SuccessResponse {
  return typeof (response as SpotifyErrorResponse).error === 'undefined';
}
